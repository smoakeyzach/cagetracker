<?php
require_once "config.php";

$app = new \Slim\Slim(array("templates.path" => __DIR__."/templates"));
$app->hook('slim.before', function () use ($app) {
    $app->view()->appendData(array("base_url" => $app->request()->getScriptName()));
});
$Page = new \CageTracker\Web\Page();

include_once "controllers/get.php";
include_once "controllers/post.php";
include_once "controllers/api.php";

/**
 * Class & DB Builders
 */
$app->get("/builder/database/", function () use ($app, $Page) {
    $Page->setPageTitle("Object Builder");
    $app->render("data_builder.php", array(
		"Page" => $Page, 
		"start_point" => "000"
	));
});
$app->get("/builder/database/:start_point", function ($start_point) use ($app, $Page) {
    $Page->setPageTitle("Data Builder");
    $app->render("data_builder.php", array(
		"Page" => $Page, 
		"start_point" => $start_point
	));
});
$app->get("/builder/:class_name", function ($class_name) use ($app, $Page) {
    $Page->setPageTitle("Object Builder");
    $app->render("builder.php", array(
		"Page" => $Page, 
		"class_name" => $class_name
	));
});

$app->run();