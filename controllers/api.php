<?php
/* @var $app \Slim\Slim */

/**
 * API Data fetches
 */
$app->get("/api/cages", function () use ($app) {
	$app->response()->header("Content-Type", "application/json");
	$Cage = new \CageTracker\Sci\Cage();
	$cages = $Cage->fetchAll();
	echo $cages->toJson();
});

$app->get("/api/cages/:id", function ($id) use ($app) {
	$app->response()->header("Content-Type", "application/json");
	$cage = new \CageTracker\Sci\Cage($id);
	echo $cage->toJson();
});

$app->get("/api/mice", function () use ($app) {
	$app->response()->header("Content-Type", "application/json");
	$Mouse = new \CageTracker\Sci\Mouse();
	$mice = $Mouse->fetchAll();
	echo $mice->toJson();
});

$app->get("/api/mice/:id", function ($id) use ($app) {
	$app->response()->header("Content-Type", "application/json");
	$mouse = new \CageTracker\Sci\Mouse($id);
	echo $mouse->toJson();
});

$app->post("/api/cages/save", function () use ($app) {
	$cageData = $app->request()->post("object");
    $cage = new \CageTracker\Sci\Cage($cageData);
	$cage->save();
});

$app->post("/api/mice/save", function () use ($app) {
	$mouseData = $app->request()->post("object");
    $mouse = new \CageTracker\Sci\Mouse($mouseData);
	$mouse->save();
});

