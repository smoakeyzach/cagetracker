<?php
/* @var $app \Slim\Slim */
/* @var $Page \CageTracker\Web\Page */

/**
 * Redirect to Index
 */
$app->get("/", function () use ($app) {
    $app->redirect("cages");
});

/**
 * API Data fetches
 */
$app->get("/api/cage/:id", function ($id) use ($app) {
	$app->response()->header("Content-Type", "application/json");
	$cage = new \CageTracker\Sci\Cage($id);
	echo $cage->toJson();
});

/**
 * Page retrieval
 */
$app->get("/v2/cages", function () use ($app, $Page) {
    $Page->setPageTitle("Cages");
	$Cage = new \CageTracker\Sci\Cage();
	$Mouse = new \CageTracker\Sci\Mouse();
    $cages = $Cage->fetchAll(array("active" => true));
    $allMice = $Mouse->fetchAll();
	$filters = $Cage->getGroupFilters($cages);
    $app->render("cages2.php", array(
		"Page" => $Page, 
		"cages" => $cages, 
		"allMice" => $allMice, 
		"filters" => $filters
	));
});

// Cage View
$app->get("/cages", function () use ($app, $Page) {
    $Page->setPageTitle("Cages");
	$Cage = new \CageTracker\Sci\Cage();
	$Mouse = new \CageTracker\Sci\Mouse();
    $cages = $Cage->fetchAll(array("active" => true));
    $allMice = $Mouse->fetchAll();
	$filters = $Cage->getGroupFilters($cages);
    $app->render("cages.php", array(
		"Page" => $Page, 
		"cages" => $cages, 
		"allMice" => $allMice, 
		"filters" => $filters
	));
});
// Litter View
$app->get("/litters", function () use ($app, $Page) {
    $Page->setPageTitle("Litters");
    $app->render("litters.php", array(
		"Page" => $Page
	));
});

/**
 * Refactor
 */
$app->get("/cages/:id", function ($id) use ($app, $Page) {
    $Page->setPageTitle("Cage : ".$id);
    $cage = new \CageTracker\Sci\Cage($id);
    $app->render("cage.php", array(
		"Page" => $Page, 
		"cage" => $cage
	));
});
$app->get("/cages/new", function () use ($app, $Page) {
    $Page->setPageTitle("New Cage");
    $cage = new \CageTracker\Sci\Cage();
    $app->render("cage.php", array(
		"Page" => $Page, 
		"cage" => $cage
	));
});
