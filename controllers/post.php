<?php
/* @var $app \Slim\Slim */
/* @var $Page \CageTracker\Web\Page */

$app->post("/cages/save", function () use ($app) {
	$cageData = $app->request()->post("object");
    $cage = new \CageTracker\Sci\Cage($cageData);
	$cage->save();
});
$app->post("/mice/move", function () use ($app) {
	$cageId = $app->request()->post("move_to_cage_id");
	$miceToMove = $app->request()->post("mice_to_move");
	
	foreach($miceToMove AS $mouseData)
	{
	    $mouse = new \CageTracker\Sci\Mouse($mouseData);
		$mouse->setCurrentCageId($cageId);
		$mouse->save();
	}
});
$app->post("/litters/add", function () use ($app) {
	$cageId = $app->request()->post("add_to_cage_id");
	$dateOfBirth = $app->request()->post("date_of_birth");
	$parentMaleId = $app->request()->post("parent_male_id");
	$parentFemaleId = $app->request()->post("parent_female_id");
	$numberOfPups = $app->request()->post("number_of_pups");
	
	$litter = new \CageTracker\Sci\Litter();
	$litter->setActive(true);
	$litter->setBirthCageId($cageId);
	$litter->setDateOfBirth($dateOfBirth);
	$litter->setParentMaleId($parentMaleId);
	$litter->setParentFemaleId($parentFemaleId);
	$litter->save();
	
	$cage = new \CageTracker\Sci\Cage($cageId);
	
	for($i = 0; $i < $numberOfPups; $i++)
	{
		$mouse = new \CageTracker\Sci\Mouse();
		$mouse->setDateOfBirth($dateOfBirth);
		$mouse->setStrain($cage->getStrain());
		$mouse->setLitterId($litter->getLitterId());
		$mouse->setCurrentCageId($cageId);
		$mouse->setParentMaleId($parentMaleId);
		$mouse->setParentFemaleId($parentFemaleId);
		$mouse->save();
	}
});
$app->post("/mice/add", function () use ($app) {
	$cageId = $app->request()->post("add_to_cage_id");
	$dateOfBirth = $app->request()->post("date_of_birth");
	$parentMaleId = $app->request()->post("parent_male_id");
	$parentFemaleId = $app->request()->post("parent_female_id");
	$numberOfPups = $app->request()->post("number_of_pups");
	
	$litter = new \CageTracker\Sci\Litter();
	$litter->setActive(true);
	$litter->setBirthCageId($cageId);
	$litter->setDateOfBirth($dateOfBirth);
	$litter->setParentMaleId($parentMaleId);
	$litter->setParentFemaleId($parentFemaleId);
	$litter->save();
	
	$cage = new \CageTracker\Sci\Cage($cageId);
	
	for($i = 0; $i < $numberOfPups; $i++)
	{
		$mouse = new \CageTracker\Sci\Mouse();
		$mouse->setDateOfBirth($dateOfBirth);
		$mouse->setStrain($cage->getStrain());
		$mouse->setLitterId($litter->getLitterId());
		$mouse->setCurrentCageId($cageId);
		$mouse->setParentMaleId($parentMaleId);
		$mouse->setParentFemaleId($parentFemaleId);
		$mouse->save();
	}
});

