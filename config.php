<?php
$display_errors = 1;
date_default_timezone_set ("America/New_York");
ini_set("display_errors", $display_errors);
ini_set("include_path", __DIR__."/lib".PATH_SEPARATOR.ini_get("include_path"));
require "vendor/autoload.php";
