var editObject;
$(document).ready(function() {
	var options = {
	  valueNames: [ "strain" ]
	};
	var cageList = new List("all_container", options);

	$(".filter").click(function() {
		$(".filter").each(function() {$(this).removeClass("active active-details");});
		cageList.search($(this).attr("filter"));
		$(this).addClass("active active-details");
	});
	
	$(".show-details").click(function() {
		$(".show-details").each(function() {$(this).removeClass("active-details");});
		$(this).addClass("active-details");
		
		var directory = (this.id).split("_")[0];
		var detail_id = (this.id).split("_")[1];
		showDetails(directory, detail_id);
	});
	
	$(".hide-details").click(function() {
		hideDetails();
	});
	
	$("#details_save").click(function() {
		var directory = this.value;
		$.each(editObject, function(key, value) {
			if($("[name=" + key + "]").length != 0)
			{
				editObject[key] = $("[name=" + key + "]").val();
			}
		});
		$.post("/" + directory + "/save", {object: editObject}, function(data){
			//$('#details_modal').modal('hide');
			hideDetails();
			location.reload(true);
		});
	});
	
	$("#mouse_new").click(function() {
		$("#mouse_parent_M_id").html("");
		$("#mouse_parent_F_id").html("");
		
		$.each(allMice, function(id, mouse) {	
			$("#mouse_parent_" + mouse.sex + "_id").append(
				$(document.createElement("option"))
					.val(mouse.mouse_id)
					.html("Mouse # : " + mouse.mouse_id + " (DOB: " + mouse.date_of_birth + ")")
			);
		});
		
		$("#add_mouse_modal").modal("show");
	});
});

function showDetails(directory, detail_id) {
	editObject = {};
	$("#details").html("Loading...");
	$("#all_container").addClass("col-md-8").addClass("col-sm-7").removeClass("col-md-12");
	$("#details_container").removeClass("hidden");

	$.get("/" + directory + "/" + detail_id, function(data){ $("#details").html(data); });
	$("#details_save").val(directory);
}

function hideDetails() {
	$("#all_container").removeClass("col-md-8").removeClass("col-sm-7").addClass("col-md-12");
	$("#details_container").addClass("hidden");
}


