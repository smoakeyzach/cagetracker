$(document).ready(function() {
	$("#cages").on("click", ".cage-details", function() {
		var cage_id = $(this).prop("cage_id");
		$.get("/api/cages/" + cage_id, function(data){ $("#details").html(data); });
	});
});
