<?php
/** @var $app \Slim\Slim */
/** @var $Page \CageTracker\Web\Page */
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo $Page->getPageTitle(); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?=$base_url?>/media/css/custom.css">
	</head>
	<body>
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">Cage Tracker</a>
				</div>
				<div class="navbar-collapse collapse navbar-form navbar-right">
					<a class="btn btn-primary" role="button" href="<?=$base_url?>/cages">Cages</a>
					<a class="btn btn-primary" role="button" href="<?=$base_url?>/litters">Litters</a>
				</div>
			</div>
		</div>