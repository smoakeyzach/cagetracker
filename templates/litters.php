<?php
include_once "base_head.php";
?>
<div class="jumbotron">
	<div class="container">
		<h1>This is the Litter Hero.</h1>
		<p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
		<p>
			<a class="btn btn-primary btn-lg" role="button" href="/cages/">Cages</a>
			<a class="btn btn-primary btn-lg" role="button" href="/litters/">Litters</a>
		</p>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-2">This is the sidebar.</div>
		<div class="col-md-10">This is the bulk of the content.</div>
	</div>
</div>
<?php
include_once "base_foot.php";
