<?php
include_once "base_head.php";
/* @var $cages \CageTracker\Sci\Cage[] */
/* @var $allMice \CageTracker\Sci\Mouse[] */
/* @var $filters array */
?>
<script type="text/javascript">
	var allMice = [
<?php
	foreach($allMice AS $allMouse)
	{
		echo $allMouse->toJson().",";
	}
?>
	];
</script>
<div class="container-fluid">
	<h2>Cages</h2>
</div>
<div class="container-fluid">
	<div class="row">
		<div id="all_container" class="col-md-12">
			<div class="row">
				<p class="pull-left side-pad">
<?php
if(isset($filters['strain']))
{
	foreach($filters['strain'] AS $strain)
	{
?>
					<button class="btn btn-info filter" filter="<?=$strain?>"><?=$strain?></button>
<?php
	}
}
?>
					<button class="btn btn-info active active-details filter" filter="">All</button>
				</p>
				<p class="pull-right side-pad">
					<button id="mouse_new" class="btn btn-success">+ Mouse</button>
					<button id="cages_new" class="btn btn-success show-details">+ Cage</button>
				</p>
			</div>
			<div class="row list">
<?php
foreach($cages AS $cage)
{
?>
				<div class="col-md-3 col-sm-4 col-xs-6">
					<div id="cages_<?=$cage->getCageId()?>" 
						 class="show-details alert alert-<?=$cage->cageStatusClass()?>">
						Cage: <?=$cage->getCageNumber()?><br />
						Strain: <span class="strain"><?=$cage->getStrain()?></span><br />
						Animals: <?=$cage->numberOfMice()?><br />
						Active Litters: <?=$cage->numberOfLitters()?>
					</div>
				</div>
<?php
}
?>
			</div>
		</div>
		<div id="details_container" class="hidden col-md-4 col-sm-5 details">
			<button type="button" class="close hide-details" data-dismiss="details" aria-hidden="true">&times;</button>
			<button id="details_save" type="button" class="btn btn-success" value="cages">Save</button>
			<div id="details"></div>
		</div>
	</div>
</div>
<?php
include_once "base_foot.php";
include_once "modals/move_mouse.php";
include_once "modals/add_litter.php";
include_once "modals/add_mouse.php";
