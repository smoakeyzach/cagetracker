<?php
/* @var $app \Slim\Slim */
/* @var $Page \CageTracker\Web\Page */
?>
		<script src="https://code.jquery.com/jquery.js"></script>
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?=$base_url?>/media/js/list.min.js"></script>
		<script type="text/javascript" src="<?=$base_url?>/media/js/custom.js"></script>
	</body>
</html>
