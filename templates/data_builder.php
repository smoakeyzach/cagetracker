<?php
include_once "base_head.php";
?>
<ul>
<?php
$sql_files = scandir("data_structure/");
foreach($sql_files as $file)
{
	list($file_order,) = explode(".", $file);
	if($file_order >= $start_point)
	{
		echo "<li>".\CageTracker\Web\Database::run_sql_file($file, true)."</li>";
	}
}
?>
</ul>
<?php
include_once "base_foot.php";
