<?php
/* @var $cage \CageTracker\Sci\Cage */
?>
<form class="form-horizontal" role="form">
	<div class="form-group">
		<label class="col-md-3 control-label">Status</label>
		<div class="col-md-9">
			<p class="form-control-static alert alert-<?php echo $cage->cageStatusClass(); ?>"><?php echo $cage->cageStatus(); ?></p>
		</div>
	</div>
	<div class="form-group">
		<label for="request_date" class="col-md-3 control-label">Request Date</label>
		<div class="col-md-9">
			<input type="date" class="form-control" name="request_date" placeholder="Date" value="<?php echo $cage->getRequestDate(); ?>">
		</div>
	</div>
	<div class="form-group">
		<label for="activation_date" class="col-md-3 control-label">Activation Date</label>
		<div class="col-md-9">
			<input type="date" class="form-control" name="activation_date" placeholder="Date" value="<?php echo $cage->getActivationDate(); ?>">
		</div>
	</div>
	<div class="form-group">
		<label for="cage_number" class="col-md-3 control-label">Cage #</label>
		<div class="col-md-9">
			<input type="number" class="form-control" name="cage_number" placeholder="Cage Number" value="<?php echo $cage->getCageNumber(); ?>">
		</div>
	</div>
	<div class="form-group">
		<label for="cage_type_gorup" class="col-md-3 control-label">Type</label>
		<div name="cage_type_group" class="col-md-9 btn-group" data-toggle="buttons">
			<label class="btn btn-default <?php if($cage->getCageType() == "small"){ echo "active";} ?>">
				<input type="radio" name="cage_type" value="small"> Small
			</label>
			<label class="btn btn-default <?php if($cage->getCageType() == "large"){ echo "active";} ?>">
				<input type="radio" name="cage_type" value="large"> Large
			</label>
		</div>
	</div>
	<div class="form-group">
		<label for="protocol_number" class="col-md-3 control-label">Protocol Number</label>
		<div class="col-md-9">
			<input type="number" class="form-control" name="protocol_number" placeholder="Protocol Number" value="<?php echo $cage->getProtocolNumber(); ?>">
		</div>
	</div>
	<div class="form-group">
		<label for="principle_investigator" class="col-md-3 control-label">P.I.</label>
		<div class="col-md-9">
			<select class="form-control" name="principle_investigator">
				<option value="M.Lampson" <?php if($cage->getPrincipleInvestigator() == "M.Lampson"){ echo "selected";} ?>>M. Lampson</option>
				<option value="R.Schultz" <?php if($cage->getPrincipleInvestigator() == "R.Schultz"){ echo "selected";} ?>>R. Schultz</option>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label for="strain" class="col-md-3 control-label">Strain</label>
		<div class="col-md-9">
			<input type="text" class="form-control" name="strain" placeholder="Strain" value="<?php echo $cage->getStrain(); ?>">
		</div>
	</div>
	<div class="form-group">
		<label for="sex_group" class="col-md-3 control-label">Sex</label>
		<div name="sex_group" class="col-md-9 btn-group" data-toggle="buttons">
			<label class="btn btn-default <?php if($cage->getSex() == "M"){ echo "active";} ?>">
				<input type="radio" name="sex" value="M"> M
			</label>
			<label class="btn btn-default <?php if($cage->getSex() == "F"){ echo "active";} ?>">
				<input type="radio" name="sex" value="large"> F
			</label>
		</div>
	</div>
	<div class="form-group">
		<label for="date_of_birth" class="col-md-3 control-label">Date Of Birth</label>
		<div class="col-md-9">
			<input type="date" class="form-control" name="date_of_birth" placeholder="Date" value="<?php echo $cage->getDateOfBirth(); ?>">
		</div>
	</div>
	<div class="form-group">
		<label for="split_reason" class="col-md-3 control-label">Split Reason</label>
		<div class="col-md-9">
			<select class="form-control" name="split_reason">
				<option value="">Not Split</option>
				<option value="mating" <?php if($cage->getSplitReason() == "mating"){ echo "selected";} ?>>Mating</option>
				<option value="research" <?php if($cage->getSplitReason() == "research"){ echo "selected";} ?>>Research</option>
				<option value="weaning" <?php if($cage->getSplitReason() == "weaning"){ echo "selected";} ?>>Weaning</option>
			</select>
		</div>
	</div>
</form>
<div class="row">
	<p class="pull-left side-pad">
		<button id="add_litter" class="btn btn-success add-litter">+ Litter</button>
	</p>
	<p class="pull-right side-pad">
		<button id="add_mouse_from_cage" class="btn btn-success move-mouse">Move Mouse</button>
	</p>
</div>
<div class="row">
	<div class="col-md-6">
		<h4>Litters</h4>
<?php
foreach($cage->getLitters() AS $litter)
{
?>
		<div class="col-md-12">
			<div id="litter_<?php echo $litter->getLitterId(); ?>" class="alert alert-info">
				Number of Pups: <?php echo $litter->numberOfPups(); ?>
			</div>
		</div>
<?php
}
?>
	</div>
	<div class="col-md-6">
		<h4>Mice</h4>
<?php
foreach($cage->getMice() AS $mouse)
{
?>
		<div class="col-md-12">
			<div id="mouse_<?php echo $mouse->getMouseId(); ?>" class="alert alert-success">
				Sex: <?php echo $mouse->getSex(); ?><br />
				Age: <?php echo $mouse->age(); ?>
			</div>
		</div>
<?php
}
?>
	</div>
</div>
<script>
	editObject = <?=$cage->toJson()?>;
	$("#add_mouse_from_cage").click(function() {
		$(".show-mice").each(function() {$(this).removeClass("hidden");});
		$("#mouse_cage_<?=$cage->getCageId()?>").addClass("hidden");
		$("#move_mouse_cage_number").html("<?=$cage->getCageNumber()?>");
		
		$("#move_mouse_modal").modal("show");
	});
	$("#add_litter").click(function() {
		$("#add_litter_cage_number").html("<?=$cage->getCageNumber()?>");
		$("#litter_parent_M_id").html("");
		$("#litter_parent_F_id").html("");
		$.each(editObject._mice, function(id, mouse) {	
			$("#litter_parent_" + mouse.sex + "_id").append(
				$(document.createElement("option"))
					.val(mouse.mouse_id)
					.html("Mouse # : " + mouse.mouse_id + " (DOB: " + mouse.date_of_birth + ")")
			);
		});
		
		$("#add_litter_modal").modal("show");
	});
</script>