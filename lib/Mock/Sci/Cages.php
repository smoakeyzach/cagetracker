<?php
/**
 * Description of Cages
 *
 * @author zsmoak
 */
namespace Mock\Sci;
class Cages {
	/**
	 * 
	 * @param int $numberOfCages
	 * @return \CageTracker\Sci\Cage[]
	 */
	public static function generateCages($numberOfCages = 1) {
		$cages = array();
		$cage_types = array("large", "small");
		$cage_status_classes = array("info", "success", "warning", "danger");
		for($i = 0; $i < $numberOfCages; $i++)
		{
			$valueArray = array(
				"cage_id" => rand(0, 100), 
				"cage_number" => rand(10000, 99999), 
				"cage_type" => $cage_types[array_rand($cage_types)], 
				"cage_status_class" => $cage_status_classes[array_rand($cage_status_classes)], 
				"_mice" => \Mock\Sci\Mice::generateMice(rand(1, 30))
			);
			
			$newCage = new \CageTracker\Sci\Cage($valueArray);
			
			$cages[] = $newCage;
		}
		
		return $cages;
	}
}
