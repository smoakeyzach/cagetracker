<?php
/**
 * Description of Mice
 *
 * @author zsmoak
 */
namespace Mock\Sci;
class Mice extends \CageTracker\Object {
	/**
	 * 
	 * @param int $numberOfMice
	 * @return \CageTracker\Sci\Mouse[]
	 */
	public static function generateMice($numberOfMice = 1) {
		$mice = array();
		for($i = 0; $i < $numberOfMice; $i++)
		{
			$valueArray = array(
			);
			
			$newMouse = new \CageTracker\Sci\Mouse();
			
			$newMouse->fromValueModel($valueArray);
			
			$mice[] = $newMouse;
		}
		
		return $mice;
	}
}
