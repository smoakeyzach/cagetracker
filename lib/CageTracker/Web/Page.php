<?php
/**
 * Description of Page
 *
 * @author zsmoak
 */
namespace CageTracker\Web;
class Page {
	/**
	 * @var string
	 */
	protected $page_title;
	/**
	 * @var string
	 */
	protected $template_page;
	/**
	 * @var \PDO
	 */
	protected $_dbConnection;
	
	/**
	 * 
	 * @param string $page_title [optional]
	 */
	public function __construct($page_title = NULL) {
		if($page_title)
		{
			$this->page_title = $page_title;
		}
	}
	
	/**
	 * 
	 * @param string $page_title
	 */
	public function setPageTitle($page_title) {
		$this->page_title = $page_title;
	}
	
	/**
	 * 
	 * @return string
	 */
	public function getPageTitle() {
		return $this->page_title;
	}
	
	/**
	 * 
	 * @param string $template_page
	 */
	public function setTemplatePage($template_page) {
		$this->template_page = $template_page;
	}
	
	/**
	 * 
	 * @return string
	 */
	public function getTemplatePage() {
		return $this->template_page;
	}
	
	/**
	 * 
	 * @param \PDO $_dbConnection
	 */
	public function setDBConnection($_dbConnection) {
		$this->_dbConnection = $_dbConnection;
	}
	
	/**
	 * 
	 * @return \PDO
	 */
	public function getDBConnection() {
		return $this->_dbConnection;
	}
}
