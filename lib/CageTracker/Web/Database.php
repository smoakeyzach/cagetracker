<?php
/**
 * Description of Database
 *
 * @author zsmoak
 */
namespace CageTracker\Web;
class Database {
	/**
	 * @var string 
	 */
	private static $_db_host = "cagetracker1.c3hulz50s8q7.us-east-1.rds.amazonaws.com";
	/**
	 * @var string 
	 */
	private static $_db_name = "cagetracker";
	/**
	 * @var string 
	 */
	private static $_db_user = "cagetracker";
	/**
	 * @var string 
	 */
	private static $_db_pass = "kznbsrc112586";
	
	/**
	 * Creates a database connection to CageTracker
	 * @return \PDO Database connection
	 */
	public static function pdo_connect_to_cagetracker() {
		return self::pdo_connect_to_db(self::$_db_name, self::$_db_host, self::$_db_user, self::$_db_pass);
	}

	/**
	 * Creates a database connection
	 * @param string $dbname Database Name
	 * @param string $dbhost Database Host
	 * @param string $dbuser Database Username
	 * @param string $dbpass Database Password
	 * @return \PDO Database connection
	 */
	public static function pdo_connect_to_db($dbname, $dbhost, $dbuser, $dbpass) {
		$db = array(
			'host' => $dbhost,
			'name' => $dbname,
			'user' => $dbuser,
			'pass' => $dbpass
		);

		$dbhost = $db['host'];
		$dbname = $db['name'];
		$dbuser = $db['user'];
		$dbpass = $db['pass'];

		$dsn = "mysql:dbname=".$dbname.";host=".$dbhost;
		try
		{
			$dbh = new \PDO($dsn, $dbuser, $dbpass);
		}
		catch (PDOException $e)
		{
			echo "Cannot connect to database! Error: {$e->getMessage()}";
			exit();
		}

		return $dbh;
	}
	
	/**
	 * @param string $file
	 * @param boolean $display [optional] Whether to display run status
	 * @return null|string
	 */
	public static function run_sql_file($file, $display = false) {
		$db = self::pdo_connect_to_cagetracker();
		$sql = file_get_contents("data_structure/".$file);
		try
		{
			$db->exec($sql);
			if($display)
			{
				return "$file successfully run.";
			}
		}
		catch(PDOException $e)
		{
			if($display)
			{
				return "$file failed. Error: {$e->getMessage()}";
			}
		}
		return null;
	}
}
