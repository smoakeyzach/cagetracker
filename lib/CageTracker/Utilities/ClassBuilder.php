<?php
/**
 * Description of Builder
 *
 * @author zsmoak
 */
namespace CageTracker\Utilities;
class ClassBuilder {
	public static function generateGetSetMethods($classname) {
		$classname = "\CageTracker\Sci\\".$classname;
		$refClass = new \ReflectionClass($classname);
		$objClass = new $classname;
		foreach($refClass->getProperties() AS $refProperty)
		{
			if(preg_match('/@var\s+([^\s]+)/', $refProperty->getDocComment(), $matches))
			{
				list(, $type) = $matches;
				$attribute = $refProperty->getName();
				echo "
					
	/**
	 * @return $type
	 */
	public function {$objClass->convertAttributeToMethodName($attribute, "get")}() {
		return \$this->{$attribute};
	}
					
	/**
	 * @param $type \$$attribute
	 */
	public function {$objClass->convertAttributeToMethodName($attribute, "set")}(\$$attribute) {
		\$this->{$attribute} = \$$attribute;
	}";
			}
		}
	}
}
