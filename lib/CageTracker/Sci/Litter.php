<?php
/**
 * Description of CageTracker_Sci_Litter
 *
 * @author zsmoak
 */
namespace CageTracker\Sci;
class Litter extends \CageTracker\Object {
	/**
	 * @var string
	 */
	protected $_tableName = "litter";
	/**
	 * @var int 
	 */
	protected $litter_id;
	/**
	 * @var boolean 
	 */
	protected $active = true;
	/**
	 * @var date 
	 */
	protected $date_of_birth;
	/**
	 * @var date 
	 */
	protected $clip_date;
	/**
	 * @var date 
	 */
	protected $wean_date;
	/**
	 * @var int 
	 */
	protected $birth_cage_id;
	/**
	 * @var int 
	 */
	protected $parent_male_id;
	/**
	 * @var int 
	 */
	protected $parent_female_id;
	/**
	 * @var \CageTracker\Sci\Mouse 
	 */
	protected $_parent_male;
	/**
	 * @var \CageTracker\Sci\Mouse 
	 */
	protected $_parent_female;
	/**
	 * @var \CageTracker\Sci\Mouse[] 
	 */
	protected $_pups;
	
					
	/**
	 * @return int
	 */
	public function getLitterId() {
		return $this->litter_id;
	}
					
	/**
	 * @param int $litter_id
	 */
	public function setLitterId($litter_id) {
		$this->litter_id = $litter_id;
	}
					
	/**
	 * @return boolean
	 */
	public function getActive() {
		return $this->active;
	}
					
	/**
	 * @param boolean $active
	 */
	public function setActive($active) {
		$this->active = $active;
	}
					
	/**
	 * @return date
	 */
	public function getDateOfBirth() {
		return $this->date_of_birth;
	}
					
	/**
	 * @param date $date_of_birth
	 */
	public function setDateOfBirth($date_of_birth) {
		$this->date_of_birth = $date_of_birth;
	}
					
	/**
	 * @return date
	 */
	public function getClipDate() {
		return $this->clip_date;
	}
					
	/**
	 * @param date $clip_date
	 */
	public function setClipDate($clip_date) {
		$this->clip_date = $clip_date;
	}
					
	/**
	 * @return date
	 */
	public function getWeanDate() {
		return $this->wean_date;
	}
					
	/**
	 * @param date $wean_date
	 */
	public function setWeanDate($wean_date) {
		$this->wean_date = $wean_date;
	}
					
	/**
	 * @return int
	 */
	public function getBirthCageId() {
		return $this->birth_cage_id;
	}
					
	/**
	 * @param int $birth_cage_id
	 */
	public function setBirthCageId($birth_cage_id) {
		$this->birth_cage_id = $birth_cage_id;
	}
					
	/**
	 * @return int
	 */
	public function getParentMaleId() {
		return $this->parent_male_id;
	}
					
	/**
	 * @param int $parent_male_id
	 */
	public function setParentMaleId($parent_male_id) {
		$this->parent_male_id = $parent_male_id;
	}
					
	/**
	 * @return int
	 */
	public function getParentFemaleId() {
		return $this->parent_female_id;
	}
					
	/**
	 * @param int $parent_female_id
	 */
	public function setParentFemaleId($parent_female_id) {
		$this->parent_female_id = $parent_female_id;
	}
					
	/**
	 * @return \CageTracker\Sci\Mouse
	 */
	public function getParentMale() {
		return $this->_parent_male;
	}
					
	/**
	 * @param \CageTracker\Sci\Mouse $_parent_male
	 */
	public function setParentMale($_parent_male) {
		$this->_parent_male = $_parent_male;
	}
					
	/**
	 * @return \CageTracker\Sci\Mouse
	 */
	public function getParentFemale() {
		return $this->_parent_female;
	}
					
	/**
	 * @param \CageTracker\Sci\Mouse $_parent_female
	 */
	public function setParentFemale($_parent_female) {
		$this->_parent_female = $_parent_female;
	}
					
	/**
	 * @return \CageTracker\Sci\Mouse[]
	 */
	public function getPups() {
		return $this->_pups;
	}
					
	/**
	 * @param \CageTracker\Sci\Mouse[] $_pups
	 */
	public function setPups($_pups) {
		$this->_pups = $_pups;
	}
	
	/**
	 * Below are the object methods
	 */
	
	protected function initialize() {
		$Mouse = new \CageTracker\Sci\Mouse();
		if(is_numeric($this->litter_id))
		{
			$this->setPups($Mouse->fetchAll(array("litter_id" => $this->litter_id)));
		}
		if(is_numeric($this->parent_female_id))
		{
			$this->setParentFemale(new \CageTracker\Sci\Mouse($this->parent_female_id));
		}
		if(is_numeric($this->parent_male_id))
		{
			$this->setParentMale(new \CageTracker\Sci\Mouse($this->parent_male_id));
		}
	}
	
	/**
	 * @return int
	 */
	public function numberOfPups() {
		return count($this->_pups);
	}
}
