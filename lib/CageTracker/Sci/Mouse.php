<?php
/**
 * Description of CageTracker_Sci_Mouse
 *
 * @author zsmoak
 */
namespace CageTracker\Sci;
class Mouse extends \CageTracker\Object {
	/**
	 * @var string
	 */
	protected $_tableName = "mouse";
	/**
	 * @var int 
	 */
	protected $mouse_id;
	/**
	 * @var date 
	 */
	protected $date_of_birth;
	/**
	 * @var date 
	 */
	protected $clip_date;
	/**
	 * @var date 
	 */
	protected $wean_date;
	/**
	 * @var date 
	 */
	protected $sacrifice_date;
	/**
	 * @var date 
	 */
	protected $experiment_date;
	/**
	 * @var text 
	 */
	protected $experiment_description;
	/**
	 * @var string 
	 */
	protected $genotype;
	/**
	 * @var string 
	 */
	protected $strain;
	/**
	 * @var enum [M|F] 
	 */
	protected $sex;
	/**
	 * @var int 
	 */
	protected $clip_number;
	
	/**
	 * @var int 
	 */
	protected $litter_id;
	/**
	 * @var int 
	 */
	protected $current_cage_id;
	/**
	 * @var int 
	 */
	protected $parent_male_id;
	/**
	 * @var int 
	 */
	protected $parent_female_id;
	/**
	 * @var \CageTracker\Sci\Mouse 
	 */
	protected $_parent_male;
	/**
	 * @var \CageTracker\Sci\Mouse 
	 */
	protected $_parent_female;
					
	/**
	 * @return int
	 */
	public function getMouseId() {
		return $this->mouse_id;
	}
					
	/**
	 * @param int $mouse_id
	 */
	public function setMouseId($mouse_id) {
		$this->mouse_id = $mouse_id;
	}
					
	/**
	 * @return date
	 */
	public function getDateOfBirth() {
		return $this->date_of_birth;
	}
					
	/**
	 * @param date $date_of_birth
	 */
	public function setDateOfBirth($date_of_birth) {
		$this->date_of_birth = $date_of_birth;
	}
					
	/**
	 * @return date
	 */
	public function getClipDate() {
		return $this->clip_date;
	}
					
	/**
	 * @param date $clip_date
	 */
	public function setClipDate($clip_date) {
		$this->clip_date = $clip_date;
	}
					
	/**
	 * @return date
	 */
	public function getWeanDate() {
		return $this->wean_date;
	}
					
	/**
	 * @param date $wean_date
	 */
	public function setWeanDate($wean_date) {
		$this->wean_date = $wean_date;
	}
					
	/**
	 * @return date
	 */
	public function getSacrificeDate() {
		return $this->sacrifice_date;
	}
					
	/**
	 * @param date $sacrifice_date
	 */
	public function setSacrificeDate($sacrifice_date) {
		$this->sacrifice_date = $sacrifice_date;
	}
					
	/**
	 * @return date
	 */
	public function getExperimentDate() {
		return $this->experiment_date;
	}
					
	/**
	 * @param date $experiment_date
	 */
	public function setExperimentDate($experiment_date) {
		$this->experiment_date = $experiment_date;
	}
					
	/**
	 * @return text
	 */
	public function getExperimentDescription() {
		return $this->experiment_description;
	}
					
	/**
	 * @param text $experiment_description
	 */
	public function setExperimentDescription($experiment_description) {
		$this->experiment_description = $experiment_description;
	}
					
	/**
	 * @return string
	 */
	public function getGenotype() {
		return $this->genotype;
	}
					
	/**
	 * @param string $genotype
	 */
	public function setGenotype($genotype) {
		$this->genotype = $genotype;
	}
					
	/**
	 * @return string
	 */
	public function getStrain() {
		return $this->strain;
	}
					
	/**
	 * @param string $strain
	 */
	public function setStrain($strain) {
		$this->strain = $strain;
	}
					
	/**
	 * @return enum
	 */
	public function getSex() {
		return $this->sex;
	}
					
	/**
	 * @param enum $sex
	 */
	public function setSex($sex) {
		$this->sex = $sex;
	}
					
	/**
	 * @return int
	 */
	public function getClipNumber() {
		return $this->clip_number;
	}
					
	/**
	 * @param int $clip_number
	 */
	public function setClipNumber($clip_number) {
		$this->clip_number = $clip_number;
	}
					
	/**
	 * @return int
	 */
	public function getLitterId() {
		return $this->litter_id;
	}
					
	/**
	 * @param int $litter_id
	 */
	public function setLitterId($litter_id) {
		$this->litter_id = $litter_id;
	}
					
	/**
	 * @return int
	 */
	public function getCurrentCageId() {
		return $this->current_cage_id;
	}
					
	/**
	 * @param int $current_cage_id
	 */
	public function setCurrentCageId($current_cage_id) {
		$this->current_cage_id = $current_cage_id;
	}
					
	/**
	 * @return int
	 */
	public function getParentMaleId() {
		return $this->parent_male_id;
	}
					
	/**
	 * @param int $parent_male_id
	 */
	public function setParentMaleId($parent_male_id) {
		$this->parent_male_id = $parent_male_id;
	}
					
	/**
	 * @return int
	 */
	public function getParentFemaleId() {
		return $this->parent_female_id;
	}
					
	/**
	 * @param int $parent_female_id
	 */
	public function setParentFemaleId($parent_female_id) {
		$this->parent_female_id = $parent_female_id;
	}
					
	/**
	 * @return \CageTracker\Sci\Mouse
	 */
	public function getParentMale() {
		return $this->_parent_male;
	}
					
	/**
	 * @param \CageTracker\Sci\Mouse $_parent_male
	 */
	public function setParentMale($_parent_male) {
		$this->_parent_male = $_parent_male;
	}
					
	/**
	 * @return \CageTracker\Sci\Mouse
	 */
	public function getParentFemale() {
		return $this->_parent_female;
	}
					
	/**
	 * @param \CageTracker\Sci\Mouse $_parent_female
	 */
	public function setParentFemale($_parent_female) {
		$this->_parent_female = $_parent_female;
	}
	
	/**
	 * Below are the object methods
	 */
	
	protected function initialize() {
		
	}
	
	public function age() {
		$birth = new \DateTime($this->date_of_birth);
		$today = new \DateTime("now");
		$age = $birth->diff($today);
		return $age->format("%a days");
	}
}
