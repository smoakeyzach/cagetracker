<?php
/**
 * Description of CageTracker_Sci_Cage
 *
 * @author zsmoak
 */
namespace CageTracker\Sci;
class Cage extends \CageTracker\Object {
	/**
	 * @var string
	 */
	protected $_tableName = "cage";
	/**
	 * @var int 
	 */
	protected $cage_id;
	/**
	 * @var int 
	 */
	protected $cage_number;
	/**
	 * @var enum [large|small] 
	 */
	protected $cage_type;
	
	/**
	 * @var enum [M.Lampson|R.Schulz] 
	 */
	protected $principle_investigator;
	/**
	 * @var enum [mating|research|weaning] 
	 */
	protected $split_reason;
	/**
	 * @var int 
	 */
	protected $protocol_number;
	/**
	 * @var date 
	 */
	protected $request_date;
	/**
	 * @var date 
	 */
	protected $activation_date;
	/**
	 * @var string 
	 */
	protected $strain;
	
	/**
	 * @var enum [M|F] 
	 */
	protected $sex;
	/**
	 * @var date 
	 */
	protected $date_of_birth;
	/**
	 * @var boolean 
	 */
	protected $active = true;
	
	/**
	 * @var CageTracker_Sci_Mouse[] 
	 */
	protected $_mice = array();
	/**
	 * @var CageTracker_Sci_Litter[] 
	 */
	protected $_litters = array();
					
	/**
	 * @return int
	 */
	public function getCageId() {
		return $this->cage_id;
	}
					
	/**
	 * @param int $cage_id
	 */
	public function setCageId($cage_id) {
		$this->cage_id = $cage_id;
	}
					
	/**
	 * @return int
	 */
	public function getCageNumber() {
		return $this->cage_number;
	}
					
	/**
	 * @param int $cage_number
	 */
	public function setCageNumber($cage_number) {
		$this->cage_number = $cage_number;
	}
					
	/**
	 * @return enum
	 */
	public function getCageType() {
		return $this->cage_type;
	}
					
	/**
	 * @param enum $cage_type
	 */
	public function setCageType($cage_type) {
		$this->cage_type = $cage_type;
	}
					
	/**
	 * @return enum
	 */
	public function getPrincipleInvestigator() {
		return $this->principle_investigator;
	}
					
	/**
	 * @param enum $principle_investigator
	 */
	public function setPrincipleInvestigator($principle_investigator) {
		$this->principle_investigator = $principle_investigator;
	}
					
	/**
	 * @return enum
	 */
	public function getSplitReason() {
		return $this->split_reason;
	}
					
	/**
	 * @param enum $split_reason
	 */
	public function setSplitReason($split_reason) {
		$this->split_reason = $split_reason;
	}
					
	/**
	 * @return int
	 */
	public function getProtocolNumber() {
		return $this->protocol_number;
	}
					
	/**
	 * @param int $protocol_number
	 */
	public function setProtocolNumber($protocol_number) {
		$this->protocol_number = $protocol_number;
	}
					
	/**
	 * @return date
	 */
	public function getRequestDate() {
		return $this->request_date;
	}
					
	/**
	 * @param date $request_date
	 */
	public function setRequestDate($request_date) {
		$this->request_date = $request_date;
	}
					
	/**
	 * @return date
	 */
	public function getActivationDate() {
		return $this->activation_date;
	}
					
	/**
	 * @param date $activation_date
	 */
	public function setActivationDate($activation_date) {
		$this->activation_date = $activation_date;
	}
					
	/**
	 * @return string
	 */
	public function getStrain() {
		return $this->strain;
	}
					
	/**
	 * @param string $strain
	 */
	public function setStrain($strain) {
		$this->strain = $strain;
	}
					
	/**
	 * @return enum
	 */
	public function getSex() {
		return $this->sex;
	}
					
	/**
	 * @param enum $sex
	 */
	public function setSex($sex) {
		$this->sex = $sex;
	}
					
	/**
	 * @return date
	 */
	public function getDateOfBirth() {
		return $this->date_of_birth;
	}
					
	/**
	 * @param date $date_of_birth
	 */
	public function setDateOfBirth($date_of_birth) {
		$this->date_of_birth = $date_of_birth;
	}
					
	/**
	 * @return boolean
	 */
	public function getActive() {
		return $this->active;
	}
					
	/**
	 * @param boolean $active
	 */
	public function setActive($active) {
		$this->active = $active;
	}
					
	/**
	 * @return \CageTracker\Sci\Mouse[]
	 */
	public function getMice() {
		return $this->_mice;
	}
					
	/**
	 * @param \CageTracker\Sci\Mouse[] $_mice
	 */
	public function setMice($_mice) {
		$this->_mice = $_mice;
	}
					
	/**
	 * @return \CageTracker\Sci\Litter[]
	 */
	public function getLitters() {
		return $this->_litters;
	}
					
	/**
	 * @param \CageTracker\Sci\Litter[] $_litters
	 */
	public function setLitters($_litters) {
		$this->_litters = $_litters;
	}
	
	/**
	 * Below are the object methods
	 */
	
	protected function initialize() {
		if(is_numeric($this->cage_id))
		{
			$Litter = new \CageTracker\Sci\Litter();
			$Mouse = new \CageTracker\Sci\Mouse();
			$this->setLitters($Litter->fetchAll(array("active" => true, "birth_cage_id" => $this->cage_id)));
			$this->setMice($Mouse->fetchAll(array("current_cage_id" => $this->cage_id)));
		}
	}
	
	/**
	 * @return string
	 */
	public function cageStatus() {
		switch($this->cageStatusClass())
		{
			case self::STATUS_BAD_CLASS:
				return "OVERDUE!";
			case self::STATUS_GOOD_CLASS:
				return "New Cage";
			case self::STATUS_NEUTRAL_CLASS:
				return "Good";
			default:
				return "Good";
		}
	}
	
	/**
	 * @return string
	 */
	public function cageStatusClass() {
		$today = date("Y-m-d");
		if(strtotime($today) >= strtotime($this->date_of_birth." + 21 days"))
		{
			return self::STATUS_BAD_CLASS;
		}
		if(strtotime($today) == strtotime($this->date_of_birth))
		{
			return self::STATUS_GOOD_CLASS;
		}
		return self::STATUS_NEUTRAL_CLASS;
	}
	
	/**
	 * @return int
	 */
	public function numberOfMice() {
		return count($this->_mice);
	}
	
	/**
	 * @return int
	 */
	public function numberOfLitters() {
		return count($this->_litters);
	}
}
