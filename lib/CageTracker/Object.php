<?php
/**
 * Base object class
 * @author zsmoak
 *
 */
namespace CageTracker;
class Object extends \CageTracker\Web\Database {
	/**
	 * @var string 
	 */
	protected $_tableName = "";
	/**
	 * @var \PDO 
	 */
	private $_dbConnection = null;
	/**
	 * @var string
	 */
	const STATUS_BAD_CLASS = "danger";
	/**
	 * @var string
	 */
	const STATUS_GOOD_CLASS = "success";
	/**
	 * @var string
	 */
	const STATUS_NEUTRAL_CLASS = "info";
	
	/**
	 * 
	 * @param int|array $options
	 */
	public function __construct($options = null) {
		$this->_dbConnection = self::pdo_connect_to_cagetracker();
		if($options)
		{
			if(is_numeric($options))
			{
				$this->fetch($options);
			}
			elseif(is_array($options))
			{
				$this->setOptions($options);
			}
		}

		$this->initialize();
    }
	
	/**
	 * 
	 * @param string $attributeName
	 * @param string $methodPrefix [optional]
	 * @return string
	 */
	public function convertAttributeToMethodName($attributeName, $methodPrefix = "") {
		$attributeNameParts = explode("_", $attributeName);
		$methodNameParts = array();
		foreach($attributeNameParts AS $namePart)
		{
			$methodNameParts[] = ucfirst($namePart);
		}
		$methodName = $methodPrefix.implode("", $methodNameParts);
		return $methodName;
	}
	
	protected function initialize() {}

	public function setOptions(array $options) {
		foreach($options as $key => $value)
		{
			$method = $this->convertAttributeToMethodName($key, "set");
			if(method_exists($this, $method))
			{
				if(in_array($value, array("true", "false")))
				{
					$value = filter_var($value, FILTER_VALIDATE_BOOLEAN);
				}
				$this->$method($value);
			}
		}
		return $this;
    }

	public function __set($name, $value) {
		$method = $this->convertAttributeToMethodName($name, "set");
		if(!method_exists($this, $method))
		{
			trigger_error("InvalidProperty ".$name, E_USER_WARNING);
			return null;
		}
		
		return $this->$method($value);
	}

	public function __get($name) {
		$method = $this->convertAttributeToMethodName($name, "get");
		if(!method_exists($this, $method))
		{
			trigger_error("InvalidProperty ".$name, E_USER_WARNING);
			return null;
		}
		
		return $this->$method();
	}
	
	/**
	 * 
	 * @param \CageTracker\Object[] $objectArray
	 * @return array
	 */
	public function getGroupFilters($objectArray) {
		$filters = array();
		if(is_array($objectArray))
		{
			foreach($objectArray AS $object)
			{
				$attributes = get_object_vars($object);
				foreach($attributes AS $attribute_name => $attribute_value)
				{
					if(!is_object($attribute_value) && $attribute_name[0] != "_")
					{
						if(!isset($filters[$attribute_name]))
						{
							$filters[$attribute_name] = array($attribute_value);
						}
						else
						{
							if(!in_array($attribute_value, $filters[$attribute_name]))
							{
								$filters[$attribute_name][] = $attribute_value;
							}
						}
					}
				}
			}
		}
		
		return $filters;
	}
	
	/**
	 * 
	 * @return string
	 */
	private function fetchPrimaryKeyName() {
		$sql = "SHOW KEYS FROM {$this->_tableName} WHERE Key_name = 'PRIMARY'";
		$sth = $this->_dbConnection->prepare($sql);
		$sth->setFetchMode(\PDO::FETCH_COLUMN, 4);
		$sth->execute();
		$primary_key_field = $sth->fetchColumn(4);
		
		return $primary_key_field;
	}
	
	/**
	 * 
	 * @param int $primary_key_id
	 */
	public function fetch($primary_key_id) {
		$primary_key_field = $this->fetchPrimaryKeyName();
		$sql = "SELECT * FROM {$this->_tableName} WHERE {$primary_key_field} = :{$primary_key_field}";
		$sth = $this->_dbConnection->prepare($sql);
		$sth->setFetchMode(\PDO::FETCH_ASSOC);
		$sth->bindValue(":{$primary_key_field}", $primary_key_id);
		$sth->execute();
		
		$objectArray = $sth->fetch();
		
		$this->setOptions($objectArray);
	}
	
	/**
	 * 
	 * @param array $parameters [optional]
	 * @return \CageTracker\Object[]
	 */
	public function fetchAll($parameters = array()) {
		$connector = "WHERE";
		$sql = "SELECT * FROM {$this->_tableName} ";
		foreach($parameters as $field => $parameter)
		{
			$sql .= "{$connector} {$field} = :{$field} ";
			$connector = "AND";
		}
		
		$sth = $this->_dbConnection->prepare($sql);
		$sth->setFetchMode(\PDO::FETCH_CLASS, get_class($this));
		
		foreach($parameters as $field => $parameter)
		{
			$sth->bindValue(":{$field}", $parameter);
		}
		
		$sth->execute();
		
		return $sth->fetchAll();
	}
	
	/**
	 * 
	 * @return json
	 */
	public function toJson() {
		return json_encode($this->getJsonData());
	}
	
	/**
	 * 
	 * @return array
	 */
	protected function getJsonData() {
		$var = get_object_vars($this);
		foreach($var as &$value)
		{
			if(is_object($value) && method_exists($value, "getJsonData"))
			{
				$value = $value->getJsonData();
			}
			if(is_array($value))
			{
				foreach($value as &$arrayItem)
				{
					if(is_object($arrayItem) && method_exists($arrayItem, "getJsonData"))
					{
						$arrayItem = $arrayItem->getJsonData();
					}
				}
			}
		}
		return $var;
	}
	
	/**
	 * 
	 * @return boolean 
	 */
	public function save() {
		$primary_key_field = $this->fetchPrimaryKeyName();
		$fields = get_object_vars($this);

		$fieldString = "";
		$valueString = "";
		$updateString = "";

		foreach($fields AS $field => $value)
		{
			if(!is_object($value) && $field[0] != "_" && $field != $primary_key_field)
			{
				$fieldString .= $field.", ";
				$valueString .= ":".$field.", ";
				$updateString .= $field." = :".$field.", ";
			}
		}

		$fieldString = rtrim($fieldString, ", ");
		$valueString = rtrim($valueString, ", ");
		$updateString = rtrim($updateString, ", ");

		$type = "insert";
		if($this->$primary_key_field > 0)
		{
			$type = "update";
		}

		if($type == "insert")
		{
		    $sql = "INSERT INTO {$this->_tableName} ({$fieldString}) VALUES ({$valueString})";
		}
		else 
		{
			$sql = "UPDATE {$this->_tableName} SET {$updateString} WHERE {$primary_key_field} = :id ";		
		}

	    $sth = $this->_dbConnection->prepare($sql);

		foreach($fields AS $field => $value)
		{
			if(!is_object($value) && $field[0] != "_" && $field != $primary_key_field)
			{
				$sth->bindValue(":".$field, $this->$field);
			}
		}

		if($type == "update")
		{
			$sth->bindParam(':id', $this->$primary_key_field, \PDO::PARAM_INT);
		}

		$result = $sth->execute();
		
		//print_r( $this->_dbConnection->errorInfo() );
		//print_r( $sth->errorInfo() );

		if($result == 1)
		{
			if($type == "insert")
			{
				$this->$primary_key_field = $this->_dbConnection->lastInsertId();
			}
			return true;
		}
		return false;
	}
}