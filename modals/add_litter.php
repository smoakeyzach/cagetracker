<?php
/* @var $cages \CageTracker\Sci\Cage[] */
?>
<div id="add_litter_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="addLitterLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="addLitterLabel">Add Litter to Cage <span id="add_litter_cage_number"></span></h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label for="litter_date_of_birth" class="col-md-3 control-label">Date Of Birth</label>
						<div class="col-md-9">
							<input id="litter_date_of_birth" type="date" class="form-control" name="litter_date_of_birth" placeholder="Date" value="<?=date("Y-d-m")?>">
						</div>
					</div>
					<div class="form-group">
						<label for="litter_parent_male_id" class="col-md-3 control-label">Parent (Male)</label>
						<div class="col-md-9">
							<select id="litter_parent_M_id" class="form-control" name="litter_parent_male_id"></select>
						</div>
					</div>
					<div class="form-group">
						<label for="litter_parent_female_id" class="col-md-3 control-label">Parent (Female)</label>
						<div class="col-md-9">
							<select id="litter_parent_F_id" class="form-control" name="litter_parent_female_id"></select>
						</div>
					</div>
					<div class="form-group">
						<label for="litter_number_of_pups" class="col-md-3 control-label">Number of Pups</label>
						<div class="col-md-9">
							<input type="number" id="litter_number_of_pups" class="form-control" name="litter_number_of_pups" />
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button id="add_litter_save" type="button" class="btn btn-success">Add</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$("#add_litter_save").click(function() {
		$.post("/litters/add", {
			add_to_cage_id: editObject.cage_id, 
			number_of_pups: $("#litter_number_of_pups").val(), 
			date_of_birth: $("#litter_date_of_birth").val(), 
			parent_male_id: $("#litter_parent_M_id").val(), 
			parent_female_id: $("#litter_parent_F_id").val()}, function(data){
				showDetails("cages", editObject.cage_id);
				$("#add_litter_modal").modal("hide");
		});
	});
</script>
