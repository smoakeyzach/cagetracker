<?php
/* @var $cages \CageTracker\Sci\Cage[] */
?>
<div id="move_mouse_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="moveMouseLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="moveMouseLabel">Move Mice to Cage <span id="move_mouse_cage_number"></span></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div id="mouse_cage_list" class="col-md-5">
<?php
foreach($cages AS $cage)
{
?>
						<div id="mouse_cage_<?php echo $cage->getCageId(); ?>" class="show-mice alert alert-info">
							Cage #: <?php echo $cage->getCageNumber(); ?><br />
							Animals: <?php echo $cage->numberOfMice(); ?><br />
						</div>
<?php
}
?>
					</div>
					<div id="mouse_list" class="col-md-7">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button id="move_mouse_save" type="button" class="btn btn-success">Move</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var mice_to_move = [];
	var current_cage_mice = [];
	$(".show-mice").click(function() {
		$(".show-mice").each(function() {$(this).removeClass("alert-success").addClass("alert-info");});
		$(this).addClass("alert-success").removeClass("alert-info");
		$("#mouse_list").html("Loading...");
		
		var directory = (this.id).split("_")[1];
		var detail_id = (this.id).split("_")[2];
		$.get("api/" + directory + "/" + detail_id, function(data){
			$("#mouse_list").html("");
			current_cage_mice = data._mice;
			$.each(data._mice, function(id, mouse) {
				$("#mouse_list").append(
					$(document.createElement("div"))
						.attr("id", "cage_mouse_" + id)
						.addClass("alert").addClass("alert-info").addClass("select-mouse")
						.html("Mouse # : " + mouse.mouse_id + " [" + mouse.sex + "]" + " (DOB: " + mouse.date_of_birth + ")")
				);
			});
		});
	});
	$("#mouse_list").on("click", ".select-mouse", function() {
		var cage_mouse_array_id = (this.id).split("_")[2];
		if($(this).hasClass("selected"))
		{
			$(this).addClass("alert-info").removeClass("alert-success").removeClass("selected");
			var remove_index = mice_to_move.indexOf(current_cage_mice[cage_mouse_array_id]);
			mice_to_move.splice(remove_index, 1);
		}
		else
		{
			$(this).removeClass("alert-info").addClass("alert-success").addClass("selected");
			mice_to_move.push(current_cage_mice[cage_mouse_array_id]);
		}
	});
	$("#move_mouse_save").click(function() {
		$.post("/mice/move", {move_to_cage_id: editObject.cage_id, mice_to_move: mice_to_move}, function(data){
			showDetails("cages", editObject.cage_id);
			$("#move_mouse_modal").modal("hide");
		});
	});
</script>
