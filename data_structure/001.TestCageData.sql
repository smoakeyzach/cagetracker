/* 2014-02-08 zsmoak */
TRUNCATE TABLE cage;
INSERT INTO cage (cage_number, cage_type, principle_investigator, protocol_number, request_date, activation_date, strain, sex, date_of_birth, active) VALUES 
	(12578, "large", "R.Schultz", "8895610", "2014-01-03", "2014-01-05", "8FT62BC", "F", "2014-01-06", true),
	(12580, "small", "M.Lampson", "9995610", "2014-01-03", "2014-01-05", "65ADBB8", "F", "2014-01-05", true),
	(12581, "small", "R.Schultz", "8895610", "2014-01-03", "2014-01-05", "8FT62BC", "F", "2014-01-05", true),
	(12582, "large", "M.Lampson", "9995610", "2014-01-03", "2014-01-05", "65ADBB8", "M", "2014-02-05", true),
	(12583, "small", "R.Schultz", "8895610", "2014-01-03", "2014-01-05", "65ADBB8", "F", "2014-02-05", true),
	(12584, "large", "R.Schultz", "8895610", "2014-01-03", "2014-01-05", "8FT62BC", "F", "2014-02-05", true),
	(12585, "small", "M.Lampson", "8895610", "2014-01-03", "2014-01-05", "65ADBB8", "F", "2014-02-05", false),
	(12586, "small", "R.Schultz", "9995610", "2014-01-03", "2014-01-05", "8FT62BC", "F", "2014-02-05", true),
	(12587, "large", "R.Schultz", "8895610", "2014-01-03", "2014-01-05", "65ADBB8", "M", "2014-02-05", true),
	(12588, "small", "M.Lampson", "9995610", "2014-01-03", "2014-01-05", "65ADBB8", "F", "2014-02-09", true);
