/* 2014-02-19 zsmoak */
TRUNCATE TABLE mouse;
INSERT INTO mouse 
(date_of_birth, clip_date, wean_date, experiment_date, experiment_description, genotype, strain, sex, litter_id, clip_number, current_cage_id, parent_male_id, parent_female_id) VALUES 
	("2014-01-12", "2014-01-20", "2014-01-25", NULL, NULL, "Type A", "8FT62BC", "M", NULL, 1, 1, NULL, NULL),
	("2014-01-12", "2014-01-20", "2014-01-25", NULL, NULL, "Type B", "8FT62BC", "F", NULL, 2, 1, NULL, NULL),
	("2014-02-12", "2014-02-20", "2014-02-25", "2014-02-30", "It was a success!", "Type B", "8FT62BC", "F", 1, 1, 1, 1, 2),
	("2014-02-12", "2014-02-20", "2014-02-25", "2014-02-30", "It was a success!", "Type B", "8FT62BC", "M", 1, 2, 1, 1, 2),
	("2014-02-12", "2014-02-20", "2014-02-25", "2014-02-30", "It was a success!", "Type B", "8FT62BC", "F", 1, 3, 1, 1, 2),
	("2014-02-12", "2014-02-20", "2014-02-25", "2014-02-30", "It was a success!", "Type B", "8FT62BC", "F", 1, 4, 1, 1, 2),
	("2014-02-12", "2014-02-20", "2014-02-25", "2014-02-30", "It was a success!", "Type B", "8FT62BC", "F", 1, 5, 1, 1, 2),
	("2014-02-12", "2014-02-20", "2014-02-25", "2014-02-30", "It was a success!", "Type B", "8FT62BC", "M", 1, 6, 1, 1, 2),
	("2014-02-12", "2014-02-20", "2014-02-25", "2014-02-30", "It was a success!", "Type B", "8FT62BC", "F", 1, 7, 1, 1, 2),
	("2014-01-12", "2014-01-20", "2014-01-25", NULL, NULL, "Type A", "8FT62BC", "M", NULL, 1, 2, NULL, NULL),
	("2014-01-12", "2014-01-20", "2014-01-25", NULL, NULL, "Type B", "8FT62BC", "F", NULL, 2, 2, NULL, NULL),
	("2014-02-12", "2014-02-20", "2014-02-25", "2014-02-30", "It was a success!", "Type B", "8FT62BC", "F", 1, 1, 2, 10, 11),
	("2014-02-12", "2014-02-20", "2014-02-25", "2014-02-30", "It was a success!", "Type B", "8FT62BC", "M", 1, 2, 2, 10, 11),
	("2014-02-12", "2014-02-20", "2014-02-25", "2014-02-30", "It was a success!", "Type B", "8FT62BC", "F", 1, 3, 2, 10, 11),
	("2014-02-12", "2014-02-20", "2014-02-25", "2014-02-30", "It was a success!", "Type B", "8FT62BC", "F", 1, 4, 2, 10, 11);
