/* 2014-02-19 zsmoak */
DROP TABLE IF EXISTS mouse;
CREATE TABLE mouse (
	mouse_id int(10) NOT NULL AUTO_INCREMENT,
	date_of_birth date NOT NULL,
	clip_date date NULL,
	wean_date date NULL,
	sacrifice_date date NULL,
	experiment_date date NULL,
	experiment_description text NULL,
	genotype varchar(255) NULL,
	strain varchar(255) NULL,
	sex enum('M','F') DEFAULT NULL,
	litter_id int(10) NULL,
	clip_number int(10) NULL,
	current_cage_id int(10) NOT NULL,
	parent_male_id int(10) NOT NULL,
	parent_female_id int(10) NOT NULL,
	PRIMARY KEY (mouse_id),
	KEY idx_date_of_birth (date_of_birth),
	KEY idx_current_cage_id (current_cage_id),
	KEY idx_parent_male_id (parent_male_id),
	KEY idx_parent_female_id (parent_female_id)
);