/* 2014-02-08 zsmoak */
DROP TABLE IF EXISTS cage;
CREATE TABLE cage (
	cage_id int(10) NOT NULL AUTO_INCREMENT,
	cage_number int(10) NOT NULL,
	cage_type enum('large','small') DEFAULT NULL,
	principle_investigator enum('M.Lampson','R.Schultz') DEFAULT 'M.Lampson',
	split_reason enum('mating','research','weaning') DEFAULT NULL,
	protocol_number int(10) NOT NULL,
	request_date date NOT NULL,
	activation_date date NULL,
	strain varchar(255) NULL,
	sex enum('M','F') DEFAULT NULL,
	date_of_birth date NULL, 
	active boolean NOT NULL DEFAULT true, 
	PRIMARY KEY (cage_id),
	UNIQUE idx_cage_number (cage_number),
	KEY idx_protocol (protocol_number),
	KEY idx_date_of_birth (date_of_birth)
);
