/* 2014-02-09 zsmoak */
DROP TABLE IF EXISTS litter;
CREATE TABLE litter (
	litter_id int(10) NOT NULL AUTO_INCREMENT,
	date_of_birth date NOT NULL,
	clip_date date NULL,
	wean_date date NULL,
	birth_cage_id int(10) NOT NULL,
	parent_male_id int(10) NOT NULL,
	parent_female_id int(10) NOT NULL,
	active boolean NOT NULL DEFAULT false, 
	PRIMARY KEY (litter_id),
	KEY idx_date_of_birth (date_of_birth),
	KEY idx_birth_cage_id (birth_cage_id),
	KEY idx_parent_male_id (parent_male_id),
	KEY idx_parent_female_id (parent_female_id)
);
